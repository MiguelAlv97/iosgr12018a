//
//  ViewController.swift
//  BullsEye
//
//  Created by Miguel Esteban Alvarez Naranjo on 24/4/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //@IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var targetLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var roundLabel: UILabel!
    
    // el slider como outlet para obtener el valor, como accion para que haga algo como controlar el brillo de la pantalla
    @IBOutlet weak var gameSlider: UISlider!
    
    var target = 0
    var score = 0
    var roundgame = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restartGame()
        
        
    }
    
    /*override func didReceiveMemoryWarning() {
     super.didReceiveMemoryWarning()
     // Dispose of any resources that can be recreated.
     }*/
    
    //play button como action
    
    @IBAction func playButton(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        
        switch sliderValue {
        case target:
            score += 100
        case (target - 2) ... (target + 2):
            score += 50
        case (target - 5) ... (target + 5):
            score += 10
        default:
            break;

        }
        
        roundgame += 1
        target = Int(arc4random_uniform(100))
        scoreLabel.text = "\(score)"
        targetLabel.text = "\(target)"
        roundLabel.text = "\(roundgame)"
        
    }
    
    

    
    @IBAction func restartButton(_ sender: Any) {
        restartGame()
    }
    
    func restartGame(){
        scoreLabel.text = "0"
        score = 0
        target = Int(arc4random_uniform(100))
        targetLabel.text = "\(target)"
        roundLabel.text = "1"
    }
    
    @IBAction func infoButtonPressed(_ sender: Any) {
    }
    
    
    @IBAction func winnerButtonPressed(_ sender: Any) {
        if score > 100{
            performSegue(withIdentifier: "toWinnerSegue", sender: self)
            
        }
    }
    
}

